using CalendarPi.Services;
using Google.Apis.Auth.AspNetCore3;
using Google.Apis.Drive.v3;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace CalendarPi.Pages
{
    [GoogleScopedAuthorize(DriveService.ScopeConstants.Drive)]
    public class NotesModel : PageModel
    {
        public QuickNotesService QuickNotesService { get; }

        public NotesModel(QuickNotesService quickNotesService)
        {
            QuickNotesService = quickNotesService;
        }

        public void OnGet()
        {

        }
    }
}
