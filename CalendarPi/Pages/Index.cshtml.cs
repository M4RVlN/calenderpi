﻿using Google.Apis.Calendar.v3;
using Google.Apis.Drive.v3;
using Google.Apis.Auth.AspNetCore3;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace CalendarPi.Pages
{
    [GoogleScopedAuthorize(CalendarService.ScopeConstants.CalendarReadonly, DriveService.ScopeConstants.Drive)]
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {

        }
    }
}
