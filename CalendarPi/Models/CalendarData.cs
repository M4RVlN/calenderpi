﻿using Google.Apis.Calendar.v3.Data;
using System;
using System.Collections.Generic;

namespace CalendarPi.Models
{
    [Serializable]
    public class CalendarData
    {
        [Serializable]
        public struct Event
        {
            [Serializable]
            public struct Time
            {
                public string Date { get; set; }
                public string DateTime { get; set; }
            }

            public ColorData.Color Colors { get; set; }
            public string Summary { get; set; }
            public Time Start { get; set; }
            public Time End { get; set; }
        }

        public Dictionary<string, Event[]> Events { get; } = new Dictionary<string, Event[]>();
    }
}
