﻿
namespace CalendarPi.Models
{
    public class RoomTemperature
    {
        public float Temperature { get; set; }
        public float Humidity { get; set; }
    }
}
