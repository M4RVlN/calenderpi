﻿using System;

namespace CalendarPi.Models
{
    [Serializable]
    public class QuickNote
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
