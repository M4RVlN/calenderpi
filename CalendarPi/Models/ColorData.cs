﻿namespace CalendarPi.Models
{
    public static class ColorData
    {
        public struct Color
        {
            public string Foreground { get; set; }
            public string Background { get; set; }

            public Color(string foreground, string background)
            {
                Foreground = foreground;
                Background = background;
            }
        }

        public static Color[] Calendar { get; } = new Color[]
        {
            new Color("#FFFFFF", "#039BE5"),
            new Color("#FFFFFF", "#795548"),
            new Color("#FFFFFF", "#E67C73"),
            new Color("#FFFFFF", "#D50000"),
            new Color("#FFFFFF", "#F4511E"),
            new Color("#FFFFFF", "#EF6C00"),
            new Color("#FFFFFF", "#F09300"),
            new Color("#FFFFFF", "#009688"),
            new Color("#FFFFFF", "#0B8043"),
            new Color("#FFFFFF", "#7CB342"),
            new Color("#FFFFFF", "#C0CA33"),
            new Color("#FFFFFF", "#E4C441"),
            new Color("#FFFFFF", "#F6BF26"),
            new Color("#FFFFFF", "#33B679"),
            new Color("#FFFFFF", "#039BE5"),
            new Color("#FFFFFF", "#4285F4"),
            new Color("#FFFFFF", "#3F51B5"),
            new Color("#FFFFFF", "#7986CB"),
            new Color("#FFFFFF", "#B39DDB"),
            new Color("#FFFFFF", "#616161"),
            new Color("#FFFFFF", "#A79B8E"),
            new Color("#FFFFFF", "#AD1457"),
            new Color("#FFFFFF", "#D81B60"),
            new Color("#FFFFFF", "#8E24AA"),
            new Color("#FFFFFF", "#9E69AF")
        };

        public static Color[] Event { get; } = new Color[]
        {
            new Color("#FFFFFF", "#039BE5"),
            new Color("#FFFFFF", "#7986CB"),
            new Color("#FFFFFF", "#33B679"),
            new Color("#FFFFFF", "#8E24AA"),
            new Color("#FFFFFF", "#E57C73"),
            new Color("#FFFFFF", "#F6BF26"),
            new Color("#FFFFFF", "#F4511E"),
            new Color("#FFFFFF", "#039BE5"),
            new Color("#FFFFFF", "#616161"),
            new Color("#FFFFFF", "#3F51B5"),
            new Color("#FFFFFF", "#0B8043"),
            new Color("#FFFFFF", "#D50000")
        };
    }
}
