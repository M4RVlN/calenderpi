﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace CalendarPi.Util
{
    /// <summary>
    /// OAuth 2.0 client ID used by this application loaded from a client ID json file.
    /// </summary>
    public class ClientInfo
    {
        private const string ClientSecretFilenameVariable = "credentials.json";

        public string ProjectId { get; }
        public string ClientId { get; }
        public string ClientSecret { get; }

        private ClientInfo(string projectId, string clientId, string clientSecret)
        {
            ProjectId = projectId;
            ClientId = clientId;
            ClientSecret = clientSecret;
        }

        public static ClientInfo Load()
        {
            var secrets = JObject.Parse(File.ReadAllText(ClientSecretFilenameVariable))["web"];

            // Check that this is a "web" client ID, not any other type of client ID like "installed app".
            // The "web" element should have been present in the json so secrets value shouldn't be null.
            if (secrets is null)
            {
                throw new InvalidOperationException(
                    $"The type of the OAuth2 client ID specified in {ClientSecretFilenameVariable} should be Web Application. You can read more about setting up OAuth2 client IDs here: https://support.google.com/cloud/answer/6158849?hl=en");
            }
            var projectId = secrets["project_id"].Value<string>();
            var clientId = secrets["client_id"].Value<string>();
            var clientSecret = secrets["client_secret"].Value<string>();

            return new ClientInfo(projectId, clientId, clientSecret);
        }
    }
}
