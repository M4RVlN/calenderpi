﻿using System;

namespace CalendarPi.Util
{
    public class CachedProperty<T>
    {
        private T _value;
        public T Value
        {
            get
            {
                if (IsExpired())
                    _value = updateAction.Invoke();

                return _value;
            }
        }

        public DateTime UpdateDate { get; private set; }
        public TimeSpan CacheDuration { get; }

        private readonly CacheUpdateAction updateAction;

        public delegate T CacheUpdateAction();

        public CachedProperty(TimeSpan cacheDuration, CacheUpdateAction action)
        {
            CacheDuration = cacheDuration;
            updateAction = action;
        }

        public CachedProperty(T value, TimeSpan cacheDuration, CacheUpdateAction action)
            : this(cacheDuration, action)
        {
            _value = value;
            UpdateDate = DateTime.Now;
        }

        public bool IsExpired()
            => UpdateDate == null || UpdateDate.Add(CacheDuration) < DateTime.Now;

        public static implicit operator T(CachedProperty<T> property)
            => property.Value;
    }
}
