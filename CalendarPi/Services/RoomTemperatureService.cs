﻿using CalendarPi.Models;
using CalendarPi.Util;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Diagnostics;
using System.IO;
using System.Text.Json;

namespace CalendarPi.Services
{
    public class RoomTemperatureService
    {
        public CachedProperty<RoomTemperature> RoomTemperatureCache { get; }

        public IWebHostEnvironment WebHostEnvironment { get; }

        public RoomTemperatureService(IWebHostEnvironment webHostEnvironment)
        {
            WebHostEnvironment = webHostEnvironment;
            RoomTemperatureCache = new CachedProperty<RoomTemperature>(new TimeSpan(0, 1, 0), FetchRoomTemperature);
        }

        private RoomTemperature FetchRoomTemperature()
        {
            //Run python script for sensor
            ProcessStartInfo start = new ProcessStartInfo
            {
                FileName = Path.Combine("Python", "temperature.py"),
                RedirectStandardOutput = true,
                CreateNoWindow = true
            };
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    var result = reader.ReadToEnd();
                    try
                    {
                        return JsonSerializer.Deserialize<RoomTemperature>(result);
                    }
                    catch (JsonException)
                    {
                        Console.WriteLine(result);
                    }
                }
            }

            return RoomTemperatureCache ?? new RoomTemperature();
        }
    }
}
