﻿using CalendarPi.Models;
using Google.Apis.Auth.AspNetCore3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Drive.v3;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Text.Json;
using System.Net.Mime;
using System;

namespace CalendarPi.Services
{
    public class QuickNotesService
    {
        public static List<QuickNote> QuickNotes { get; set; }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment WebHostEnvironment { get; }

        private IGoogleAuthProvider GoogleAuth { get; }

        public QuickNotesService(IWebHostEnvironment webHostEnvironment, IConfiguration configuration, [FromServices] IGoogleAuthProvider googleAuth)
        {
            WebHostEnvironment = webHostEnvironment;
            Configuration = configuration;
            GoogleAuth = googleAuth;

            if (QuickNotes == null)
                Deserialize();
        }

        public void AddNote(QuickNote note)
        {
            if (note == null)
                return;

            QuickNotes.Add(note);

            Serialize();
        }

        public void DeleteNote(int id)
        {
            if (id < 0 || id >= QuickNotes.Count)
                return;

            QuickNotes.RemoveAt(id);

            Serialize();
        }

        public void UpdateNote(int id, QuickNote note)
        {
            if (id < 0 || id >= QuickNotes.Count || note == null)
                return;

            var qn = QuickNotes[id];
            qn.Title = note.Title;
            qn.Content = note.Content;

            Serialize();
        }

        private async void Serialize()
        {
            GoogleCredential credential = await GoogleAuth.GetCredentialAsync();
            var googleService = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential
            });

            var stream = new MemoryStream();
            await JsonSerializer.SerializeAsync(stream, QuickNotes);
            googleService.Files.Update(new Google.Apis.Drive.v3.Data.File(), Configuration["API:QuickNotesFileID"], stream, MediaTypeNames.Application.Json);
        }

        private void Deserialize()
        {
            GoogleCredential credential = GoogleAuth.GetCredentialAsync().Result;
            var googleService = new DriveService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential
            });

            var query = googleService.Files.Get(Configuration["API:QuickNotesFileID"]);
            var stream = new MemoryStream();
            query.Download(stream);

            try
            {
                var reader = new StreamReader(stream);
                QuickNotes = JsonSerializer.Deserialize<List<QuickNote>>(reader.ReadToEnd());
            }
            catch (Exception)
            {
                QuickNotes = new List<QuickNote>();
            }
        }
    }
}
