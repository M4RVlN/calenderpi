﻿using CalendarPi.Models;
using CalendarPi.Services;
using Google.Apis.Auth.AspNetCore3;
using Google.Apis.Drive.v3;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace CalendarPi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [GoogleScopedAuthorize(DriveService.ScopeConstants.Drive)]
    public class NotesController : ControllerBase
    {
        private QuickNotesService QuickNotesService { get; }

        public NotesController(QuickNotesService quickNotesService)
        {
            QuickNotesService = quickNotesService;
        }

        [HttpGet]
        public List<QuickNote> Get()
        {
            return QuickNotesService.QuickNotes;
        }

        [HttpGet("{id}")]
        public QuickNote Get(int id)
        {
            if (id < 0 || id >= QuickNotesService.QuickNotes.Count)
                return null;

            return QuickNotesService.QuickNotes[id];
        }

        [HttpPut("{id}")]
        public bool Put(int id, [FromBody] string value)
        {
            try
            {
                if (id < 0 || id > QuickNotesService.QuickNotes.Count)
                    return false;

                var note = JsonSerializer.Deserialize<QuickNote>(value);

                if (id < QuickNotesService.QuickNotes.Count)
                    QuickNotesService.UpdateNote(id, note);
                else
                    QuickNotesService.AddNote(note);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        [HttpDelete("{id}")]
        public bool Delete(int id)
        {
            if (id < 0 || id >= QuickNotesService.QuickNotes.Count)
                return false;

            QuickNotesService.DeleteNote(id);

            return true;
        }
    }
}
