﻿using Google.Apis.Services;
using Google.Apis.Auth.AspNetCore3;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Mvc;
using System;
using CalendarPi.Models;
using Google.Apis.Calendar.v3;

namespace CalendarPi.Controllers
{
    [Route("api/events")]
    [ApiController]
    [GoogleScopedAuthorize(CalendarService.ScopeConstants.CalendarReadonly)]
    public class CalendarController : ControllerBase
    {
        [HttpGet]
        public CalendarData Get([FromServices] IGoogleAuthProvider googleAuth)
        {
            try
            {
                GoogleCredential credential = googleAuth.GetCredentialAsync().Result;
                var googleService = new CalendarService(new BaseClientService.Initializer
                {
                    HttpClientInitializer = credential
                });

                var calendarData = new CalendarData();

                var date = DateTime.Now;
                var startDate = new DateTime(date.Year, date.Month, date.Day);
                var endDate = date.Add(new TimeSpan(7, 0, 0, 0));

                var calendarList = googleService.CalendarList.List().Execute();

                foreach (var calendar in calendarList.Items)
                {
                    //Skip week day calendar
                    if (calendar.Id.StartsWith("e_2_de#weeknum"))
                        continue;

                    var query = googleService.Events.List(calendar.Id);
                    query.TimeMin = startDate;
                    query.TimeMax = endDate;
                    query.SingleEvents = true;

                    var result = query.Execute();
                    if (result.Items.Count > 0)
                    {
                        //Add event data
                        var events = new CalendarData.Event[result.Items.Count];
                        for (int i = 0; i < result.Items.Count; i++)
                        {
                            var e = result.Items[i];

                            events[i] = new CalendarData.Event()
                            {
                                Colors = e.ColorId == null ? ColorData.Calendar[Convert.ToInt32(calendar.ColorId)] : ColorData.Event[Convert.ToInt32(e.ColorId)],
                                Summary = e.Summary,
                                Start = new CalendarData.Event.Time()
                                {
                                    Date = e.Start.Date,
                                    DateTime = e.Start.DateTime?.ToString("s")
                                },
                                End = new CalendarData.Event.Time()
                                {
                                    Date = e.End.Date,
                                    DateTime = e.End.DateTime?.ToString("s")
                                }
                            };
                        }
                        calendarData.Events.Add(calendar.Id, events);
                    }
                }
                return calendarData;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
