﻿import Adafruit_DHT as dht
import time
import board
import json

METRIC_UNITS = True
DHT_PIN = 4

try:
    humidity, temp_c = dht.read_retry(dht.DHT22, DHT_PIN)
    out = {
        "Temperature": temp_c,
        "Humidity": humidity
    }
    print json.dumps(out)
except RuntimeError:
    print "[Python] RuntimeError: Could not read temperature sensor value"